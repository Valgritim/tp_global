package salaires;

import java.util.Date;

public class Vendeur extends FrontOffice{
	
	private static int count;
	
	public Vendeur(String nom, String prenom, int age, Date dateEntree, int chiffreAffaire) {
		super(nom, prenom, age, dateEntree, chiffreAffaire);
		count++;
		
	}

	public static int getCount() {
		return count;
	}
	

	
	public String getNom() {
		return "L'employ� " + prenom + " " +nom;
		
	};

	@Override
	public Double calculerSalaire() {
		Double salaire = (this.getChiffreAffaire() * 0.20) + 400.00;
		return salaire;
		//System.out.println("le salaire de " + this.getNom() + " " + "est de " + salaire + "Francs");
	}

}
