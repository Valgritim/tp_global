package salaires;

import java.util.Date;

public class Manutention extends Employe {
	
	private Double heuresTravail;
	private static int count;

	public Manutention(String nom, String prenom, int age, Date dateEntree, Double heuresTravail) {
		super(nom, prenom, age, dateEntree);
		this.heuresTravail = heuresTravail;
		++count;
	}	
	
	public static int getCount() {
		return count;
	}


	public Double getHeuresTravail() {
		return heuresTravail;
	}

	public void setHeuresTravail(Double heuresTravail) {
		this.heuresTravail = heuresTravail;
	}

	@Override
	public Double calculerSalaire() {
		Double salaire = this.getHeuresTravail() * 65;
		return salaire;
		//System.out.println("le salaire de " + this.getNom() + " " + this.getPrenom() + " " + "est de " + salaire + "Francs");
	}

}
