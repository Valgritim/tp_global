package salaires;

import java.util.*;

public class Personnel {

	private List<Employe> listPersonnel;

	public Personnel(List<Employe> listPersonnel) {
		super();
		this.listPersonnel = listPersonnel;
	}

	public List<Employe> getListPersonnel() {
		return listPersonnel;
	}

	public void setListPersonnel(List<Employe> listPersonnel) {
		this.listPersonnel = listPersonnel;
	}
	
	

	@Override
	public String toString() {
		return "Personnel [listPersonnel=" + listPersonnel + "]";
	}

	public boolean ajouterEmploye(List<Employe> employes,Employe employe) {
		return employes.add(employe);
	}
	
	public void afficherListe(List<Employe> employes) {
		for(int i = 0; i < employes.size(); i++)
			System.out.println(employes.get(i).getNom() + " gagne " + employes.get(i).calculerSalaire());
		
	}
	
	public void salaireMoyen(List<Employe> employes) {
		Double total = 0.00;		
		for(int i = 0; i < employes.size(); i++) {
			Double salaireTotal = employes.get(i).calculerSalaire();
			total += salaireTotal;			
		}		
		Double moyenne = total/(employes.size());
		System.out.println("Le salaire moyen est: " + moyenne);
	}
	
}
