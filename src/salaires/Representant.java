package salaires;

import java.util.Date;

public class Representant extends FrontOffice {
	
	private static int count;


	public Representant(String nom, String prenom, int age, Date dateEntree, int chiffreAffaire) {
		super(nom, prenom, age, dateEntree, chiffreAffaire);
		++count;
	}

	public static int getCount() {
		return count;
	}
	

	@Override
	public Double calculerSalaire() {
		Double salaire = this.getChiffreAffaire() * 0.20 + 800;
		return salaire;
		//System.out.println("le salaire de " + this.getNom() + " " + this.getPrenom() + " " + "est de " + salaire + "Francs");
	}

}
