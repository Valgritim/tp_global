package salaires;

import java.util.Date;

public class FrontOffice extends Employe {
	private int chiffreAffaire;
	private static int count;
	
	public FrontOffice(String nom, String prenom, int age, Date dateEntree,int chiffreAffaire) {
		super(nom, prenom, age, dateEntree);
		this.chiffreAffaire = chiffreAffaire;
		++count;
	}
	

	public int getChiffreAffaire() {
		return chiffreAffaire;
	}


	public void setChiffreAffaire(int chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}


	public static int getCount() {
		return count;
	}


	@Override
	public Double calculerSalaire() {
		// TODO Auto-generated method stub
		return null;
	}


}
