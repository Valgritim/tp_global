package salaires;

import java.util.Date;

public class ManuRisque extends Manutention implements EmployesRisque{

	public ManuRisque(String nom, String prenom, int age, Date dateEntree, Double heuresTravail) {
		super(nom, prenom, age, dateEntree, heuresTravail);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public Double calculerSalaire() {
		Double salaire = this.getHeuresTravail() * 65 + primeMensuelle;
		return salaire;
		//System.out.println("le salaire de " + this.getNom() + " " + this.getPrenom() + " " + "est de " + salaire + "Francs");
	}

}
