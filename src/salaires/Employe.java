package salaires;

import java.util.Date;

public abstract class Employe {
	
	protected String nom;
	protected String prenom;
	protected int age;
	protected Date dateEntree;
	private static int count;
	
	public Employe(String nom, String prenom, int age, Date dateEntree) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateEntree = dateEntree;
		++count;
	}
	
	
	public static int getCount() {
		return count;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getDateEntree() {
		return dateEntree;
	}

	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}



	public abstract Double calculerSalaire();
	
	public String getNom() {
		return "L'employ� " + prenom + " " + nom;
		
	};
	
	

}
