package salaires;

import java.util.Date;

public class ProdRisque extends Production implements EmployesRisque{
	
	
	public ProdRisque(String nom, String prenom, int age, Date dateEntree, int nbUnite) {
		super(nom, prenom, age, dateEntree, nbUnite);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Double calculerSalaire() {
		Double salaire = (double) (this.getNbUnite() * 5 + primeMensuelle);
		return salaire;
		//System.out.println("le salaire de " + this.getNom() + " " + this.getPrenom() + " " + "est de " + salaire + "Francs");
	}
}
