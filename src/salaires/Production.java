package salaires;

import java.util.Date;

public class Production extends Employe {
	
	private int nbUnite;
	private static int count;

	public Production(String nom, String prenom, int age, Date dateEntree, int nbUnite) {
		super(nom, prenom, age, dateEntree);
		this.nbUnite = nbUnite;
		++count;
	}	


	public static int getCount() {
		return count;
	}

	public int getNbUnite() {
		return nbUnite;
	}

	public void setNbUnite(int nbUnite) {
		this.nbUnite = nbUnite;
	}

	@Override
	public Double calculerSalaire() {
		Double salaire = (double) (this.getNbUnite() * 5);
		// System.out.println("le salaire de " + this.getNom() + " " + this.getPrenom() + " " + "est de " + salaire + "Francs");
		return salaire;
	}

}
