package salaires.main;

import java.util.*;

import salaires.Employe;
import salaires.ManuRisque;
import salaires.Manutention;
import salaires.Personnel;
import salaires.ProdRisque;
import salaires.Production;
import salaires.Representant;
import salaires.Vendeur;

public class Main {

	public static void main(String[] args) {
		
		Manutention employe = new ManuRisque("Delcros","Alexis",23, new Date("02/01/2020"),45.00);
		Production employe2 = new Production("Einstein", "Albert", 72, new Date("02/01/2019"),1000);
		Vendeur employe3 = new Vendeur("Musk","Elon",45, new Date("05/25/1996"), 30000);
		Representant employe4 = new Representant("Gates", "Bill", 57, new Date("05/25/1996"),20000);
		Production employe5 = new ProdRisque("Einstein2", "Albert2", 72, new Date("02/01/2019"),1000);
		Manutention employe6 = new Manutention("Musk2","Elon2",45, new Date("05/25/1996"), 45.00);
		
		List<Employe> employes = new ArrayList<Employe>();
		
		Personnel personnel = new Personnel(employes);
		personnel.ajouterEmploye(employes, employe);
		personnel.ajouterEmploye(employes, employe2);
		personnel.ajouterEmploye(employes, employe3);
		personnel.ajouterEmploye(employes, employe4);
		personnel.ajouterEmploye(employes, employe5);
		personnel.ajouterEmploye(employes, employe6);
		
		
		personnel.afficherListe(employes);
		personnel.salaireMoyen(employes);
	
		

	}

}
